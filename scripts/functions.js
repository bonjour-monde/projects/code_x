//TODO :
/// check imported html
/// act on image before building pages
/// loading pageElement -> start polyfill when everything is ready


$(document).ready(function(){

	class MyHandler extends Paged.Handler {
		constructor(chunker, polisher, caller) {
			super(chunker, polisher, caller);
		}

		afterPageLayout(pageElement, page, breakToken) {
			// console.log(pageElement)
			if(pageElement != undefined){
				var links = $(pageElement).find("p:not(.biblio_entry) a");
			}
			//
			var length = links.length;
			links.each(function(index){
				if(this != undefined){
					var url = $(this).attr("href");
					url = url.trim();
					console.log(url)
					if($(this).attr("class") == "back"){
						$(this).after( "<div class='marker'>╝</div><div class='urlback'><div>╨── </div><a href='"+url+"'>"+url+"</a></div>" );
					}else{
						$(this).after( "<div class='marker'>╝</div><div class='url'><div>╨── </div><a href='"+url+"'>"+url+"</a></div>" );
					}

				}
			});

		}
	}
	Paged.registerHandlers(MyHandler);

	$.ajax({
		context: this,
		dataType : "html",
		url : "http://51.254.121.46:9001/p/code-x/export/txt",
		success : function(results) {
			// ajout des crochets
			results = results.replace(/(\[)/gu, '<span class="croch">[');
			results = results.replace(/(\])/gu, ']</span>');
			$('#content').html(results);

			myfilter = function(node){
			// console.log(node.parentNode.tagName);
			  if (node.parentNode.tagName=="P"){
				  return NodeFilter.FILTER_ACCEPT
			  }else{
				  return NodeFilter.FILTER_SKIP;
			  }


			}

			/*NBSP MOTHERFUCKER*/
			/*https://raw.githubusercontent.com/DimNC/NBSP/master/nbsp.js*/
			var walker = document.createTreeWalker(document.body, NodeFilter.SHOW_TEXT, myfilter, false);

			while (walker.nextNode()) {
			  //  // Non-breaking spaces after numbers.
			  walker.currentNode.nodeValue = walker.currentNode.nodeValue.replace(/(\d)\s/gu, '$1\u00A0');
			  //  // Non-breaking spaces before ';' ':' '!' and '?'.
			  walker.currentNode.nodeValue = walker.currentNode.nodeValue.replace(/\s([;:!?])/gu, '\u202F$1');
			  // // Thin spaces around parentheses
			  walker.currentNode.nodeValue = walker.currentNode.nodeValue.replace(/([(])/gu, '$1');
			  walker.currentNode.nodeValue = walker.currentNode.nodeValue.replace(/([)])/gu, '$1');
			  // // En-spaces around en dashes
			  walker.currentNode.nodeValue = walker.currentNode.nodeValue.replace(/\s([–])\s/gu, '\u2002$1\u2002');
			  // // French guillemets with thin spaces
			  walker.currentNode.nodeValue = walker.currentNode.nodeValue.replace(/([«])\s/g, '$1\u202F');
			  walker.currentNode.nodeValue = walker.currentNode.nodeValue.replace(/\s([»])/g, '\u202F$1');

		  }
			$('body').imagesLoaded( function() {
				$("#loading").remove();

	  			var images = $("html").find("img:not(#headersommaire)");
	  			var gal = $("html").find(".gallery img");
				var intertitres = $("html").find(".tourni");
				// var counter = 0;
				var legends =[
'Nicolas frespech & PAMAL, <i>Les Secrets</i>, installation, "Une archéologie des média", Seconde Nature, Aix-en-Provence, 2015. Crédits&nbsp: Frespech & PAMAL.',

'Nicolas frespech & PAMAL, <i>Les Secrets</i>, installation, "Une archéologie des média", Seconde Nature, Aix-en-Provence, 2015 (détail). Crédits&nbsp: Frespech & PAMAL.',

'PAMAL, <i>3615 Love</i>, test, 2018. Crédits&nbsp: PAMAL.',

'De haut en bas et de gauche à droite : Terminal brésilien Videotexto privé, 1984; Terminal Vidéotex Thomson utilisé lors de l\'expérience Télétel de Vélizy en 1980 en France ; Exemple d\'une page vidéotex sur les services Videotexto (service aéroportuaire) ; Terminal brésilien Videotexto public, 1985. Crédits&nbsp: D.R.',

'Soirée d\'ouverture de l\'exposition <i>Brazil High-Tech</i>. Crédits&nbsp: Eduardo Kac.',

'Méthode de retranscription manuelle des <i>Videotext Poems</i> dans un code hexadécimal. Crédits&nbsp: PAMAL.',

'<br/> <br/>Eduardo Kac & PAMAL, <i>Videotext Poems</i> en deux versions : second original avec Minitel couleur et serveur simulé (1985-86) et projection vidéo (2015), Seconde Nature, Aix-en-Provence, 2015. Crédits&nbsp: Kac & PAMAL. ',

'PAMAL, micro-serveur Minitel, Frankenstein Média, Médiathèque Ceccano, Avignon, 2015. Crédits&nbsp: PAMAL.',

'PAMAL, installation permettant de consulter un second original des <i>Videotext Poems</i>, exposition <i>Frankenstein Média</i>, Médiathèque Ceccano, Avignon, 2015. Crédits&nbsp: PAMAL.',

'Eduardo Kac & PAMAL, <i>Videotext Poems</i> (second original, serveur simulé) & PAMAL, cartel, Palais de Tokyo, Paris, 2016. Crédits&nbsp: Kac, PAMAL.',

'Eduardo Kac & PAMAL, <i>Videotext Poems</i> (second original, serveur simulé) & PAMAL, cartel, Palais de Tokyo, Paris, 2016. Crédits&nbsp: Kac, PAMAL.',

'Eduardo Kac, <i>Videotext Poems</i>, version réinterprétée sans serveur (vidéo PAMAL), MoMA, New York, 2016. Crédits&nbsp: Kac.',

'Jacques-Elie Chabert et Camille Philibert, <i>L\'Objet perdu</i>, sérigraphie, 1985. Crédits&nbsp: Chabert-Philibert, PAMAL. ',

'Étude de <i>L\'Objet perdu</i>, sérigraphie, 1985. Crédits&nbsp: PAMAL.',

'Jacques-Elie Chabert et Camille Philibert & PAMAL,  <i>L\'Objet perdu</i>, second original, 1985. Crédits&nbsp: Chabert-Philibert, PAMAL.',

'Cartel d\'exposition, Musée du Quai Branly, Paris, 2016. Crédits&nbsp: PAMAL. ',

'PAMAL, <i>Cartel</i>, 2015. Crédits&nbsp: PAMAL. ',

'PAMAL, <i>Cartel</i>, 2015. Crédits&nbsp: PAMAL. ',

'PAMAL, Présentation de l\'exposition <i>Une archéologie des média</i>, Seconde Nature, Aix-en-Provence, 2015. Crédits&nbsp: Seconde Nature, PAMAL.',

'Albertine Meunier & PAMAL, <i>Angelino</i>, versions restaurée et non-restaurée, Palais de Tokyo, Paris, 2016. Crédits&nbsp: Meunier, PAMAL. ',

'PAMAL, <i>3615 LOVE</i>, 2018. Crédits&nbsp: PAMAL.',

'Jacques-Elie Chabert et Camille Philibert & PAMAL, <i>L\'Objet perdu</i>, second original, 1985. Crédits&nbsp;: Chabert-Philibert, PAMAL.',

'Eduardo Kac & PAMAL, <i>Videotext Poems</i>, second original, 1984, serveur WIPITEL1, Cerisy-la-Salle, 2018. Crédits&nbsp: Kac, PAMAL.',

'Jacques-Elie Chabert et Camille Philibert & PAMAL, <i>L\'Objet perdu</i>, second original, 1985, Cerisy-la-Salle, 2018. Crédits&nbsp: Chabert-Philibert, PAMAL.',

'Jerome Saint-Clair, <i>Dead Minitel Orchestra</i>, Performance, Cerisy-la-Salle, 2018. Crédits&nbsp: Saint-Clair, PAMAL.',

'Jerome Saint-Clair, <i>Internet m\'a tuer</i>, Cerisy-la-Salle, 2018. Crédits&nbsp;: Saint-Clair, Haute.',

'Crédits&nbsp: PAMAL.',

'Crédits&nbsp: PAMAL.',

'Fonctionnement du WIPITEL 1. Crédits&nbsp;: PAMAL',

'Boitier du WIPITEL 1. Crédits&nbsp;: PAMAL',

'Infrastructure Wipitel. Crédits&nbsp;: PAMAL.',

'3615 CHAT, portail Wipibook, PAMAL, 2018. Crédits&nbsp;: PAMAL.' ,

'PAMAL, <i>3615CHAT</i>, test, 2018. Crédits&nbsp;: PAMAL.',

'PAMAL, WIPITEL1 (interface). Crédits&nbsp;: PAMAL',

'PAMAL, 3615CHAT, test, 2018. Crédits&nbsp;: PAMAL.',

'3615 CHAT, portail Wipibook, PAMAL, 2018. Crédits&nbsp;: PAMAL.',

'Capture d\'écran du programme <i>Arbomaker</i>, PAMAL, 2018 (détail). Crédits&nbsp;: PAMAL.',

'Capture d\'écran du programme <i>Arbomaker</i>, PAMAL, 2018. Crédits&nbsp;: PAMAL.',

'Capture d\'écran du programme <i>AVItoVDT</i>, PAMAL, 2018. Crédits&nbsp;: PAMAL.',

'Capture d\'écran du programme <i>JPGtoVDT</i>, PAMAL, 2018. Crédits&nbsp;: PAMAL.',

]
	  			images.each(function(){
	  				if($(this).data("num") != undefined){
						var num = $(this).data("num");
	  					$(this).wrap( "<span class='image_holder')></span>" );
						$(this).parent().append("<span>"+legends[num-1]+"</span>");
	  				}
	  			});
				intertitres.each(function(){
					var characters = $(this).text().split("");
					$this = $(this);
					$this.empty();
					$.each(characters, function (i, el) {
					    $this.append("<span style='display:inline-block;margin-bottom:"+Math.floor(Math.random() * 10)+"px'>" + el + "</span");
					});
	  			});
				$('p').hyphenate('fr');

				$(".biblio_entry").each(function(){
					var rNum = (Math.random()*5)-2;
				  $(this).css( {
					'-webkit-transform': 'rotate('+rNum+'2deg)',
					'-moz-transform': 'rotate('+rNum+'2deg)'
				  } );
				});
				window.PagedPolyfill.preview();
			});
		}
	});


});
